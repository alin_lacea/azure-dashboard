from django.db import models

# Create your models here.
class Account(models.Model):
    id = models.AutoField(primary_key=True)
    account_name = models.CharField(max_length=50, unique=True)
    account_key = models.CharField(max_length=100)
    
    def __unicode__(self):
        return self.account_name
