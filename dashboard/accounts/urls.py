from django.conf.urls import patterns, url
from accounts import views

urlpatterns = patterns('',
                       url(r'^$', views.accounts_list),
                       url(r'(?P<id>[0-9]+)/$', views.account_detail),
                       #url(r'^(?P<account_name>\w+)/$', views.index, name="index")
                       )
